@extends('layouts.app')

@section('content')
<div class="container">
    <a href="{{ url('/configure-roles') }}" type="button" class="btn btn-primary">Configurar Roles</a>
    <a href="{{ url('/configure-permissions') }}" type="button" class="btn btn-primary">Configurar Permisos</a>
    <permission />
</div>
@endsection
