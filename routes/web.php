<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\PermissionController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/configure-roles', [App\Http\Controllers\RoleViewController::class, 'index'])->name('role');
Route::get('/configure-permissions', [App\Http\Controllers\PermissionViewController::class, 'index'])->name('permission');

Route::apiResource('/roles', RoleController::class);
Route::apiResource('/permissions', PermissionController::class);